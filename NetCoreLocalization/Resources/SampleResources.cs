﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DbLocalizationProvider;
using DbLocalizationProvider.Abstractions;

namespace NetCoreLocalization.Resources
{
    [LocalizedResource]
    public class SampleResources
    {
        public static string SomeCommonText => "Hello World!";

        [TranslationForCulture("Selamat Datang","my")]
        public string PageHeader => "Welcome";
    }
}
