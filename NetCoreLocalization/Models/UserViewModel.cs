﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DbLocalizationProvider;
using System.ComponentModel.DataAnnotations;

namespace NetCoreLocalization.Models
{
    [LocalizedModel]
    public class UserViewModel
    {
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Display(Name = "ID No")]
        public string IDNo { get; set; }
    }
}
