﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NetCoreLocalization.Models;
using Microsoft.Extensions.Localization;
using NetCoreLocalization.Resources;

namespace NetCoreLocalization.Controllers
{
    public class HomeController : Controller
    {
        private readonly IStringLocalizer<SampleResources> _localizer;

        public HomeController(IStringLocalizer<SampleResources> localizer)
        {
            _localizer = localizer;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult User()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
